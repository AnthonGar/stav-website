<?php
    include_once "php/dbc.php"
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Access Database</title>
</head>
<body>

    <?php
        $query = "SELECT * FROM users;";
        $result = mysqli_query($conn, $query);
        $resultCheck = mysqli_num_rows($result);
        if ($resultCheck > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                echo $row['username'] . '<br>';
            }
        }
    ?>
    
</body>
</html>