# stav-website
**In order for the website to work you need to install MAMP from this link:**
https://www.mamp.info/en/downloads/

MAMP should take care of MySQL server, Nginx/Apache, and PHP.  
**After downloading MAMP:**
1. Go to: MAMP -> Preferences... -> Web Server
2. Change the "Document Root" to the correct folder.

**if you have MySQL server other then the MAMP's one, do the following:**
1. add 'mysqladmin' (located at 'C:\Program Files\MySQL\MySQL Server 8\bin) to the system environment variable, Path.
2. reboot your computer
3. open cmd and input the command: "mysqladmin -u root -p shutdown"

**Here are some refs that will help you:**
* variables in AJAX: https://www.w3schools.com/js/js_const.asp 
* forms, error messages and basic handling: https://github.com/dcode-youtube/js-ajax-form-submission
* AJAX sample code: https://www.youtube.com/redirect?redir_token=QUFFLUhqa0NWallBaVFOYjJpUURkR3dwakZWRzNzNy0xZ3xBQ3Jtc0ttbXpGOGFvUXJ5TWNUcHI3MndVd2RobW4tN3NQbmdoTnVhQUNqRnJtMUNaTURQOVgxQ0o2UEp1cFJDZlVDeHo4STBYQzhTZUN1dmRQOFpOSU9VTnBNVnFPMEdQaldrVkhfQkdwV19DaHBOMHdTaEJuUQ%3D%3D&q=http%3A%2F%2Fwww.traversymedia.com%2Fdownloads%2Fajaxcrash.zip&event=video_description&v=82hnvUYY6QA  
* PHP and database: https://www.youtube.com/redirect?q=http%3A%2F%2Fgoo.gl%2FAocylf&v=mpQts3ezPVg&redir_token=QUFFLUhqa1UtWFBqanZHZ1JuWkVKdkhBVVkzbUo4LWNoZ3xBQ3Jtc0trdnV3STFOb0Y5bHd3MEtDNHhPTUVRTlFUMEhoMHRWcXMwRWhfX05tOVh2ZnVpODRLR3lVdkd1SkdlcTB6UzZNYmhsQTdhaTlKMFNfQjFMamZvZDVkWjZlWVJTdUw2UHo4T1ViYUd4QUoxaG15UTk4SQ%3D%3D&event=video_description  https://www.youtube.com/watch?v=XhLAB1wwzNk  https://www.youtube.com/watch?v=0YLJ0uO6n8I