<?php
    $first_name = isset($_POST['first_name']) ? $_POST["first_name"] : "";
    $last_name = isset($_POST['last_name']) ? $_POST["last_name"] : "";
    $email = isset($_POST['email']) ? $_POST["email"] : "";
    $pref = isset($_POST['pref']) ? $_POST["pref"] : "";

    $clear = true;
    $errors = array();

    if (!isset($first_name) || empty($first_name)){
        $ok = false;
        $errors[] = 'First name cannot be empty!';
    }

    if (!isset($last_name) || empty($last_name)){
        $ok = false;
        $errors[] = 'Last name cannot be empty!';
    }

    if (!isset($email) || empty($email)){
        $ok = false;
        $errors[] = 'Email cannot be empty!';
    }

    echo json_encode(
        array(
            'ok' => $ok,
            'errors' => $errors
        )
    );
?>